package com.example.lelab.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lelab.R;
import com.example.lelab.network.model.UserModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.HeroViewHolder> {

    Context mContext;
    List<UserModel> userList;

    private final UserListAdapter.Capture oCapture;

    public UserListAdapter(Context context, List<UserModel> userList, UserListAdapter.Capture capture) {
        this.mContext = context;
        this.userList = userList;
        this.oCapture = capture;
    }

    @NonNull
    @Override
    public HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_list_item, parent, false);
        return new HeroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroViewHolder holder, int position) {
        final UserModel userModel = userList.get(position);

        holder.tvName.setText(userModel.getName());
        holder.tvPhone.setText(userModel.getPhone());
        holder.tvLocation.setText(userModel.getAddress().getCity() + "\n" + userModel.getAddress().getStreet());

        if ((userModel.getId() & 1) == 0) {
            holder.imgPicker.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_place_black_24dp));
            holder.imgThumbnail.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.back_blue));
            holder.imgAvatar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.avatar_2));
        }
        else {
            holder.imgPicker.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_place_24dp));
            holder.imgThumbnail.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.back_purple));
            holder.imgAvatar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.avatar_1));
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oCapture.getUserPositionClick(userModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        int arr = 0;
        try {
            if (userList.size() == 0) {
                arr = 0;
            } else {
                arr = userList.size();
            }
        } catch (Exception e) {

        }
        return arr;
    }

    class HeroViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvPhone, tvLocation;
        RelativeLayout layout;
        ImageView imgPicker, imgThumbnail, imgAvatar;


        public HeroViewHolder(View itemView) {
            super(itemView);

            imgAvatar = itemView.findViewById(R.id.imgAvatar);
            imgThumbnail = itemView.findViewById(R.id.imgThumbnail);
            tvName = itemView.findViewById(R.id.tvName);
            tvPhone = itemView.findViewById(R.id.tvPhone);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            layout = itemView.findViewById(R.id.layoutUser);
            imgPicker = itemView.findViewById(R.id.imgLocation);
        }
    }

    public interface Capture {
        void getUserPositionClick(UserModel data);
    }

}
