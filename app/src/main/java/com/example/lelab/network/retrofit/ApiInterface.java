package com.example.lelab.network.retrofit;

import com.example.lelab.network.model.UserModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
//    String BASE_URL = "https://jsonplaceholder.typicode.com/";

    @GET("users")
    Call<List<UserModel>> getUsersData();
}
