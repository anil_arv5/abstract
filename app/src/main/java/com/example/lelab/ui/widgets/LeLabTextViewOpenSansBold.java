package com.example.lelab.ui.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.example.lelab.utils.FontConstants;
import androidx.appcompat.widget.AppCompatTextView;

public class LeLabTextViewOpenSansBold extends AppCompatTextView {

    public LeLabTextViewOpenSansBold(Context context) {
        super(context);
        init(null);
    }

    public LeLabTextViewOpenSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public LeLabTextViewOpenSansBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), FontConstants.OPEN_SANS_BOLD);
        setTypeface(myTypeface);
    }
}

