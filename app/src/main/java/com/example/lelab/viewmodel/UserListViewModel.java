package com.example.lelab.viewmodel;

import com.example.lelab.network.model.UserModel;
import com.example.lelab.network.retrofit.ApiInterface;
import com.example.lelab.utils.AppConstants;
import java.util.List;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserListViewModel extends ViewModel {

    private MutableLiveData<List<UserModel>> usersListData;

    public LiveData<List<UserModel>> getHeroes() {
        if (usersListData == null) {
            usersListData = new MutableLiveData<List<UserModel>>();
            loadUsersData();
        }
        return usersListData;
    }

    private void loadUsersData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Call<List<UserModel>> call = api.getUsersData();

        call.enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {

                usersListData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<UserModel>> call, Throwable t) {

            }
        });
    }
}
