package com.example.lelab.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lelab.R;
import com.example.lelab.network.model.UserModel;
import com.example.lelab.utils.AppConstants;
import com.example.lelab.utils.Util;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class ProfileDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private UserModel userModel;
    private String phoneNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile_detail);

        Intent intent = getIntent();
        if (intent.hasExtra(AppConstants.USERS_DATA)) {
            Gson gson = new Gson();
            userModel = gson.fromJson(getIntent().getStringExtra(AppConstants.USERS_DATA), UserModel.class);
            updateUI(userModel);
        }

        toolbarBackButton().setOnClickListener(this);
        getPhoneText().setOnClickListener(this);
    }

    private void updateUI(UserModel userModel) {

        Animation animateLayout = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.go_up_anim);
        getDetailLayout().startAnimation(animateLayout);
        getEditText().startAnimation(animateLayout);
        getProfilePic().startAnimation(animateLayout);

        if ((userModel.getId() & 1) == 0) {
            getProfilePic().setImageDrawable(ContextCompat.getDrawable(ProfileDetailActivity.this, R.drawable.avatar_2));
            getFrameLayoutHeader().setBackground(ContextCompat.getDrawable(ProfileDetailActivity.this, R.drawable.back_blue));
        } else {
            getProfilePic().setImageDrawable(ContextCompat.getDrawable(ProfileDetailActivity.this, R.drawable.avatar_1));
            getFrameLayoutHeader().setBackground(ContextCompat.getDrawable(ProfileDetailActivity.this, R.drawable.back_purple));
        }

        /* Binding Header name & company name*/
        if (!TextUtils.isEmpty(userModel.getName()))
            getNameText().setText(userModel.getName());
        if (!TextUtils.isEmpty(userModel.getCompany().getName())) {

            getCompanyNameText().setText(userModel.getCompany().getName());

            String companyName = "<b>" + "Name : " + "</b> " + userModel.getCompany().getName();
            getCompanyNameCardText().setText(Html.fromHtml(companyName));

        }

        /*  Binding Phone, email & website text*/
        if (!TextUtils.isEmpty(userModel.getPhone())) {
            getPhoneText().setText(userModel.getPhone());
            phoneNumber = userModel.getPhone();
        }
        if (!TextUtils.isEmpty(userModel.getEmail()))
            getEmailText().setText(userModel.getEmail());
        if (!TextUtils.isEmpty(userModel.getWebsite())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                getWebsiteText().setText(Html.fromHtml(userModel.getWebsite(), Html.FROM_HTML_MODE_COMPACT));
            }
        }

         /* Binding Company catch-phrase & Bs*/
        if (!TextUtils.isEmpty(userModel.getCompany().getCatchPhrase())) {
            String catchPhrase = "<b>" + "Catch Phrase : " + "</b> " + userModel.getCompany().getCatchPhrase();
            getCatchPhraseText().setText(Html.fromHtml(catchPhrase));
        }
        if (!TextUtils.isEmpty(userModel.getCompany().getBs())) {
            String bs = "<b>" + "Bs : " + "</b> " + userModel.getCompany().getBs();
            getBsText().setText(Html.fromHtml(bs));
        }

          /*Binding Address data*/
        if (!TextUtils.isEmpty(userModel.getAddress().getCity()) && !TextUtils.isEmpty(userModel.getAddress().getStreet())) {
            getMapLocationText().setText(userModel.getAddress().getCity() + "\n" + userModel.getAddress().getStreet());
        }
        if (!TextUtils.isEmpty(userModel.getAddress().getStreet())) {
            String street = "<b>" + "Street : " + "</b> " + userModel.getAddress().getStreet();
            getStreetText().setText(Html.fromHtml(street));
        }
        if (!TextUtils.isEmpty(userModel.getAddress().getStreet())) {
            String suite = "<b>" + "Suite : " + "</b> " + userModel.getAddress().getSuite();
            getSuiteText().setText(Html.fromHtml(suite));
        }
        if (!TextUtils.isEmpty(userModel.getAddress().getZipcode())) {
            String zipCode = "<b>" + "Zip-code : " + "</b> " + userModel.getAddress().getZipcode();
            getZipCodeText().setText(Html.fromHtml(zipCode));
        }

    }

     /*Typecasting components*/
    private ImageView toolbarBackButton() {
        return (ImageView) findViewById(R.id.line_back);
    }
    private FrameLayout getFrameLayoutHeader() { return (FrameLayout) findViewById(R.id.frameBackground); }
    private CoordinatorLayout rootLayout() { return (CoordinatorLayout) findViewById(R.id.rootDetails); }

    private ImageView getProfilePic() {
        return (ImageView) findViewById(R.id.imgProfilePic);
    }
    private TextView getNameText() {
        return (TextView) findViewById(R.id.tvProfileName);
    }
    private TextView getCompanyNameText() {
        return (TextView) findViewById(R.id.tvCompanyName);
    }
    private TextView getPhoneText() {
        return (TextView) findViewById(R.id.tvPhone);
    }
    private TextView getEmailText() {
        return (TextView) findViewById(R.id.tvEmail);
    }
    private TextView getWebsiteText() {
        return (TextView) findViewById(R.id.tvWebsite);
    }
    private TextView getCompanyNameCardText() { return (TextView) findViewById(R.id.tvCompanyNameC); }
    private TextView getCatchPhraseText() { return (TextView) findViewById(R.id.tvCompanyCatchPhrase); }

    private TextView getBsText() {
        return (TextView) findViewById(R.id.tvCompanyBs);
    }
    private TextView getMapLocationText() {
        return (TextView) findViewById(R.id.tvLocationMap);
    }
    private TextView getStreetText() {
        return (TextView) findViewById(R.id.tvStreet);
    }
    private TextView getSuiteText() {
        return (TextView) findViewById(R.id.tvSuite);
    }
    private TextView getZipCodeText() {
        return (TextView) findViewById(R.id.tvZipCode);
    }
    private TextView getEditText() {
        return (TextView) findViewById(R.id.tvEdit);
    }
    private LinearLayout getDetailLayout() { return (LinearLayout) findViewById(R.id.layoutDetailCard); }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.line_back:
                finish();
                break;

            case R.id.tvPhone:
                if (isPermissionGranted())
                    callContactService(phoneNumber);
                break;
        }
    }

    /*call phone number intent*/
    public void callContactService(final String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Util.showSnackBarSuccess(rootLayout(), "Permission granted");
                    callContactService(phoneNumber);
                } else {
                    Util.showSnackBarError(rootLayout(), "Permission denied");
                }
                return;
            }
        }
    }
}
