package com.example.lelab.utils;

public class FontConstants {

    public static final String OPEN_SANS_REGULAR = "Fonts/OpenSans-Regular.ttf";
    public static final String OPEN_SANS_LIGHT = "Fonts/OpenSans-Light.ttf";
    public static final String OPEN_SANS_BOLD = "Fonts/OpenSans-Bold.ttf";
    public static final String OPEN_SANS_SEMI_BOLD = "Fonts/OpenSans-Semibold.ttf";
    public static final String OPEN_SANS_LIGHT_ITALIC = "Fonts/OpenSans-LightItalic.ttf";
    public static final String PLAY_FAIR_DISPLAY_BOLD = "Fonts/PlayfairDisplay-Bold.otf";
    public static final String PLAY_FAIR_DISPLAY_REGULAR = "Fonts/PlayfairDisplay-Regular.otf";
}
