package com.example.lelab.utils;

public class AppConstants {

    public static final String BASE_URL = "https://jsonplaceholder.typicode.com/";
    public static final String USERS_LIST = "users_list";
    public static final String USERS_DATA = "users_data";
}
