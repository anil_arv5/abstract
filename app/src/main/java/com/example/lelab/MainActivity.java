package com.example.lelab;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.lelab.adapter.UserListAdapter;
import com.example.lelab.database.TinyDB;
import com.example.lelab.network.model.UserModel;
import com.example.lelab.ui.activity.ProfileActivity;
import com.example.lelab.utils.AppConstants;
import com.example.lelab.utils.Util;
import com.example.lelab.viewmodel.UserListViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MainActivity extends AppCompatActivity implements UserListAdapter.Capture {

    UserListAdapter adapter;
    List<UserModel> userList = new ArrayList<>();
    private UserListAdapter.Capture userCapture;
    private final String CHANNEL_ID = "welcome_notification2";
    private TinyDB tinyDB;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        userCapture = this;
        tinyDB = new TinyDB(MainActivity.this);

        /*Welcome notification trigger*/
        displayLocalNotification();

        if (Util.isNetworkAvailable(MainActivity.this)) {
            setUpViewModel();
        } else {
            setUpOfflineMode();
            Util.showSnackBarError(rootLayout(), getResources().getString(R.string.no_network));
        }

        getSwipeRefreshLayout().setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (getSwipeRefreshLayout().isRefreshing())
                    getSwipeRefreshLayout().setRefreshing(false);
                if (Util.isNetworkAvailable(MainActivity.this)) {
                    setUpViewModel();
                } else {
                    setUpOfflineMode();
                    Util.showSnackBarError(rootLayout(), getResources().getString(R.string.no_network));
                }
            }
        });
    }

    private void setUpViewModel() {
        UserListViewModel viewModel = ViewModelProviders.of(this).get(UserListViewModel.class);
        viewModel.getHeroes().observe(this, new Observer<List<UserModel>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onChanged(@Nullable List<UserModel> heroList) {
                if (getProgressBar() != null && getProgressBar().isAnimating())
                    getProgressBar().setVisibility(View.GONE);

                userList.clear();
                userList.addAll(heroList);
                putListObject(AppConstants.USERS_LIST, userList);
                setUpRecyclerAdapter(userList);
            }
        });
    }

    /*set up response data to recyclerview*/
    private void setUpRecyclerAdapter(List<UserModel> objectList) {

        getUsersRecyclerView().setHasFixedSize(true);
        getUsersRecyclerView().setLayoutManager(new LinearLayoutManager(this));
        adapter = new UserListAdapter(MainActivity.this, objectList, userCapture);
        getUsersRecyclerView().setAdapter(adapter);
    }

    /*set up response data to recyclerview offline*/
    private void setUpOfflineMode() {

        getProgressBar().setVisibility(View.GONE);
        userList = getListNotices(AppConstants.USERS_LIST);
        if (userList.size() > 0)
            setUpRecyclerAdapter(userList);
    }

    /*Type casting*/
    private RecyclerView getUsersRecyclerView() { return (RecyclerView) findViewById(R.id.usersRecyclerView); }
    private ProgressBar getProgressBar() {
        return (ProgressBar) findViewById(R.id.progressBar);
    }
    private CoordinatorLayout rootLayout() { return (CoordinatorLayout) findViewById(R.id.rootLayout); }
    private SwipeRefreshLayout getSwipeRefreshLayout() { return (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout); }

    @Override
    public void getUserPositionClick(UserModel data) {

        Intent i = new Intent(MainActivity.this, ProfileActivity.class);
        Gson gson = new Gson();
        String myJson = gson.toJson(data);
        i.putExtra(AppConstants.USERS_DATA, myJson);
        startActivity(i);
    }

    /*set up local notification*/
    private void displayLocalNotification() {

        Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(MainActivity.this,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_place_black_24dp);
        builder.setContentTitle("Welcome");
        builder.setContentText("It's good to see you back!" + " " + getEmojiByUnicode(0x1F642));
        builder.setCategory(NotificationCompat.CATEGORY_CALL);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setAutoCancel(true);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.CYAN);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert manager != null;
            builder.setChannelId(CHANNEL_ID);
            manager.createNotificationChannel(notificationChannel);
        }
        manager.notify(1, builder.build());
    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    /*Write response list for offline*/
    public void putListObject(String key, List<UserModel> objArray) {
        tinyDB.checkForNullKey(key);
        Gson gson = new Gson();
        ArrayList<String> objStrings = new ArrayList<String>();
        for (Object obj : objArray) {
            objStrings.add(gson.toJson(obj));
        }
        tinyDB.putListString(key, objStrings);
    }

    /*Read response list for offline*/
    public ArrayList<UserModel> getListNotices(String key) {
        Gson gson = new Gson();

        ArrayList<String> objStrings = tinyDB.getListString(key);
        ArrayList<UserModel> playerList = new ArrayList<UserModel>();

        for (String jObjString : objStrings) {
            UserModel player = gson.fromJson(jObjString, UserModel.class);
            playerList.add(player);
        }
        return playerList;
    }

}
