package com.example.lelab.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lelab.R;
import com.example.lelab.network.model.UserModel;
import com.example.lelab.utils.AppConstants;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private UserModel userModel;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);

        Intent intent = getIntent();
        if (intent.hasExtra(AppConstants.USERS_DATA)) {
            Gson gson = new Gson();
            userModel = gson.fromJson(getIntent().getStringExtra(AppConstants.USERS_DATA), UserModel.class);
            updateUI(userModel);
        }
        toolbarBackButton().setOnClickListener(this);
        getProfileText().setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void updateUI(UserModel ob) {
        if ((ob.getId() & 1) == 0)
            mainLayout().setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.mountain_girl));
        else
            mainLayout().setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.hiking_woman));

        String[] separated = ob.getName().split(" ");
        getText().setText(separated[0].trim() + "\n" + separated[1].trim());

        Animation animateNameText = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        getText().startAnimation(animateNameText);
        getProfileText().startAnimation(animateNameText);

    }

    private RelativeLayout mainLayout() {
        return (RelativeLayout) findViewById(R.id.relativeMain);
    }
    private TextView getText() {
        return (TextView) findViewById(R.id.tvName);
    }
    private TextView getProfileText() {
        return (TextView) findViewById(R.id.tvProfile);
    }
    private ImageView toolbarBackButton() {
        return (ImageView) findViewById(R.id.line_back);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.line_back:
                finish();
                break;

            case R.id.tvProfile:
                callProfileDetailIntent();
                break;
        }
    }

    private void callProfileDetailIntent(){
        Intent i = new Intent(ProfileActivity.this, ProfileDetailActivity.class);
        Gson gson = new Gson();
        String myJson = gson.toJson(userModel);
        i.putExtra(AppConstants.USERS_DATA, myJson);
        startActivity(i);
        finish();
    }
}
