package com.example.lelab.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;

import com.example.lelab.R;
import com.google.android.material.snackbar.Snackbar;

public class Util {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo netInfo = connectivity.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected();
        }
    }

    public static void showSnackBarError(View view, String text) {
        Snackbar snackbar = Snackbar
                .make(view, text, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(view.getResources().getColor(R.color.red));
        snackbar.setActionTextColor(view.getResources().getColor(R.color.white));
        snackbar.show();
    }

    public static void showSnackBarSuccess(View view, String text) {
        Snackbar snackbar = Snackbar
                .make(view, text, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(view.getResources().getColor(R.color.green));
        snackbar.setActionTextColor(view.getResources().getColor(R.color.white));
        snackbar.show();
    }

}
