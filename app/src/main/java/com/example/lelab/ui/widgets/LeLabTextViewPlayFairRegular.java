package com.example.lelab.ui.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.lelab.utils.FontConstants;

import androidx.appcompat.widget.AppCompatTextView;

public class LeLabTextViewPlayFairRegular extends AppCompatTextView {

    public LeLabTextViewPlayFairRegular(Context context) {
        super(context);
        init(null);
    }

    public LeLabTextViewPlayFairRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public LeLabTextViewPlayFairRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), FontConstants.PLAY_FAIR_DISPLAY_REGULAR);
        setTypeface(myTypeface);
    }
}
